# Welcome to the MHx230 Owner's Manual

Thank you for purchasing the MHx230 Computing Platform.

![The MHx230 Logo](images/x230.png)

Your MHx230 laptop hardware is a refurbished [Lenovo Thinkpad X230](https://www.thinkwiki.org/wiki/Category:X230) model. 
The BIOS has been replaced with [coreboot](https://www.coreboot.org/Board:lenovo/x230) and 
the [Intel ME](https://www.coreboot.org/Intel_Management_Engine)
has also been crippled. The wireless card that runs in your laptop is 
now [Atheros](https://wireless.wiki.kernel.org/en/users/drivers/atheros) based and hence, requires no binary 
[firmware blob](https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi). The default OS loaded on your laptop is 
[PureOS](https://pureos.net/). 
But you are encouraged to re-install the OS yourself
and choose disk encryption for greater security. PureOS and [Debian](https://debian.org) are good operating systems to use.

# Skulls - the Coreboot distribution for X230

[Skulls](https://github.com/merge/skulls) is a project to make it easier to use Coreboot with the X230 laptop. In other words, 
Skulls is a *coreboot distribution* tailored for the Thinkpad X230 laptop.
The project provides pre-built coreboot images that can be flashed onto the BIOS chips directly. Coreboot on your MHx230
laptop has been flashed using builds provided by the Skulls project.

## Powered by mkdocs

This manual is powered by `mkdocs`. You can know more about mkdocs on
the [mkdocs website](https://mkdocs.org). It uses the
`mkdocs-material` theme and other markdown extensions that are described
on the [corresponding
homepage](https://squidfunk.github.io/mkdocs-material/).

## Source code

The source code of this wiki is tracked via git on [our
Gitlab](https://dev.easypush.in/abhas/x230).

