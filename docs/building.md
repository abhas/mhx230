# Building this documentation

## Using this repository

This repository contains the source code for the MHx230 Owner's Manual. Here are
instructions on how to develop or test or use the documentation locally on your
own computer.

### Install Python and `python-pip`

The `mkdocs` system is built on Python. To build the documentation or browse it
locally, you need to have python install on your system. Additionally, you need
`python-pip` to install Python packages.

### Install `mkdocs` and related plugins

Once you have both Python and `python-pip` installed, you can install mkdocs
using the following commands. Additionaly, we use the Material theme for
mkdocs. Running the following command installs both mkdocs as well as the
corresponding theme:

``` bash
pip install mkdocs-material
```

You can more details about this on the mkdocs-material home page: https://squidfunk.github.io/mkdocs-material/

### Run a local webserver

You compile the documentation from the source code as follows:

``` bash
mkdocs build
```

This will generate the static HTML documentation into the `site/`
directory.

You can also view the documentation using the in-built webserver by running the
following cammdn:

``` bash
$ mkdocs serve
INFO    -  Cleaning site directory 
[I 171121 15:24:50 server:283] Serving on http://127.0.0.1:8000
[I 171121 15:24:50 handlers:60] Start watching changes
[I 171121 15:24:50 handlers:62] Start detecting changes
```

